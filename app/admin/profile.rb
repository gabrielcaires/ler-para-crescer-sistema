ActiveAdmin.register Profile do
    menu false
    # Associations
    belongs_to :child , parent_class: Child,:optional =>true
    #   
    filter :child,  :if => proc { params[:child_id].nil? }


 
    # Active Admin Bug using Nested Form =(
    controller do
      def permitted_params
        params.permit!
      end
    end

    index do
      selectable_column

      column :child if params[:child_id].nil?
      column "Autor" do |profile|
        if current_member.admin? 
          auto_link profile.member.name
        else
          profile.member.name
        end
      end
      column :created_at do |profile|
        profile.created_at.strftime('%d/%b/%Y %h')
      end
      default_actions
    end


    show do |profile|
      attributes_table do
       row :child
       row "Autor" do |profile|
          if  profile.member.nil?
            "Autor desconhecido"
          else
            auto_link profile.member
          end
       end
       row :created_at
       row :updated_at

      end

      panel "Respostas" do 
        render :partial=> "answers.haml", locals:{answers: profile.answers}
      end
    end

    # Views
    form do |f|
      f.inputs "Informações" do
        f.input :child, :input_html => { :disabled => true }
        f.input :member_id, :as=> :hidden, :input_html => {:value => current_member.id}
        f.has_many :answers,new_record: false, allow_destroy:false do |fa|
          fa.input :question, :input_html => { :disabled => true } do 
            fa.question.label
          end
          # fa.input :answerable
          # fa.input :id, :as=> :hidden 
          fa.input :value
          fa.input :member_id, :as=> :hidden, :input_html => {:value => current_member.id}
          fa.input :question_id,:as=> :hidden do 
             fa.question.id
          end

        end
        # f.input :created_at, as: 'date'
        f.input :tag_list
        f.actions
      end
    end  

  
end
