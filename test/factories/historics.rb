# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :historic do
  	tag_list  'tag1, tag2'
  	member
  	educational_center
  end
end
