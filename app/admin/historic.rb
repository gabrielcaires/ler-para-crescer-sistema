ActiveAdmin.register Historic do
  # Permitions 
  # permit_params :tag_list, :member_id, :child, :description,:id,:answers_attributes=>[:yq], :educational_center_id
  
  # Associations
  belongs_to :child, parent_class: Child , :optional => ->(){current_member.admin?}
  #   
  filter :child,  :if => proc { params[:child_id].nil? }
  filter :member
  filter :tags

  # Active Admin Bug using Nested Form =(
  controller do
      def permitted_params
        params.permit!
      end
    end

  index do
    selectable_column
    column :child if params[:child_id].nil?
    column "Autor" do |historic|
      if current_member.admin? 
        auto_link historic.member.name
      else
        historic.member.name
      end
    end
    column :updated_at
    default_actions
  end


  show do |historic|
    attributes_table do
     row :child
     row "Autor" do |historic|
        if  historic.member.nil?
          "Autor desconhecido"
        else
          auto_link historic.member
        end
     end
     row :created_at
     row :updated_at

    end

    panel "Respostas" do 
      render :partial=> "historic_answers.haml", locals:{answers: historic.answers}
    end
  end

  # Views
  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs "Informações" do
      f.input :child, :input_html => { :disabled => true }
      f.input :member_id, :as=> :hidden, :input_html => {:value => current_member.id}
      f.has_many :answers,new_record: false, new_record:false, allow_destroy:false do |fa|
        fa.input :question, :input_html => { :disabled => true } do 
          fa.question.label
        end
        # fa.input :answerable
        # fa.input :id, :as=> :hidden 
        fa.input :value
        fa.input :member_id, :as=> :hidden, :input_html => {:value => current_member.id}
        fa.input :question_id,:as=> :hidden do 
           fa.question.id
        end

      end
    
      f.input :tag_list
      f.actions
    end
  end  

  
end

