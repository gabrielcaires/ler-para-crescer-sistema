class AddAttachmentFileToMember < ActiveRecord::Migration
  def change
  	 add_attachment :members, :file
  end
end
