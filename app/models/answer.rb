class Answer < ActiveRecord::Base
 belongs_to :question
 belongs_to :historic
 belongs_to :profile
 belongs_to :answerable, polymorphic: true
 belongs_to :member
 belongs_to :child

 validates :question, presence: true
 # validates :member, presence: true

 
 def answered?
 	return (self.value.blank? == false) 
 end



end

