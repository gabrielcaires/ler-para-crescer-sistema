ActiveAdmin.register Member do

 
  permit_params :name, :role, :utf8, :description,:authenticity_token,:skill_list, :commit, :file,:address,:admin, :birthdate,:phone,:status,:email,:facebook,:cell1,:cell2,:cell3,:avatar,:email, :password, :password_confirmation
  filter :name
  filter :educational_center
  # filter :role, coll
  filter :status
  filter :email
  filter :skills

  scope :actives
  scope :unactives
  scope :all

  


  # Batch actions 
  batch_action :active, :priority=> 0, :if => proc {params['scope'] != "actives"} do |selection|
    Member.find(selection).each do |member|
      member.active
   
    end
    redirect_to :back 
  end

  batch_action :deactive, :priority=> 1, :if => proc {params['scope'] != "unactives"} do |selection|
    Member.find(selection).each do |member|
      member.deactive
    end
    redirect_to :back 
  end

  batch_action :destroy, :if => proc {current_member.admin?}
 

  index do
    selectable_column
    column :avatar do |member|
      image_tag(member.avatar.url(:thumb)) unless member.avatar.blank?
    end

    column :name
    column :educational_center
    column :email
    column :phone
    column :status do |member|
       active_status_tag(member.status)
     end
    default_actions
  end
  
  show do |member|
     attributes_table do
        row :avatar do 
          image_tag(member.avatar.url(:thumb)) unless member.avatar.blank?
        end
        row :name
        row :address
        row :birthdate
        row :phone
        row :status do
            active_status_tag(member.status)
        end
        row :email
        row :facebook
        row :phone
        row :cell1
        row :cell2
       
        row :file do 
          link_to "Baixar curriculo",member.file.url if member.file?
        end
    end
  end

  form :multipart => true do |f|
    f.inputs "Detalhes" do
      f.input :avatar, :as => :file, :hint => (f.template.image_tag(f.object.avatar.url(:thumb)) if f.object.avatar.file?)
      f.input :name
      f.input :email
      f.input :file, label: "Curriculo", :hint => (if f.object.file? then f.object.file.name else "Arquivo somente pode ser dos tipos Word(.doc,.docx), PDF(.pdf) ou Texto(.txt,.rtf)" end)
      f.input :skill_list, :hint => "descreva os talentos separado por virgula (talento1, talento2)"
      f.input :address
      f.input :birthdate, :start_year=> 1900
      f.input :status if current_member.role.admin? 
      f.input :facebook
      f.input :phone
      f.input :cell1
      f.input :cell2
      f.input :description if current_member.role.admin? 
      f.input :role, :collection => Member.role.options if current_member.role.admin? 
      f.input :password
      f.input :password_confirmation

    end
    f.actions
  end


  # Controller
  controller do
      def update
        params[:member][:skill_list] = params[:member][:skill_list].downcase
        if params[:member][:password].blank?
          params[:member].delete("password")
          params[:member].delete("password_confirmation")
        end
        super
      end

      def create
        params[:member][:skill_list] = params[:member][:skill_list].downcase
        random =  Array.new(32){rand(36).to_s(36)}.join
        if params[:member][:password].blank?
          params[:member][:password] = random
          params[:member][:password_confirmation] = random
        end
        super
      end
    end

end
