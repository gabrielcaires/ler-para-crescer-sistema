class AddChildToAnswer < ActiveRecord::Migration
  def change
    add_reference :answers, :child, index: true
  end
end
