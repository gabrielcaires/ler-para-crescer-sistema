ActiveAdmin.register_page "Dashboard" do
  # TPDP: Implement warnning of children
  menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }

  content :title => proc{ I18n.t("active_admin.dashboard") } do
    # div :class => "blank_slate_container", :id => "dashboard_default_message" do
    #   span :class => "blank_slate" do
    #     span I18n.t("active_admin.dashboard_welcome.welcome")
    #     small I18n.t("active_admin.dashboard_welcome.call_to_action")
    #   end
    # end

    # Here is an example of a simple dashboard with columns and panels.
    #
    # TODO Fazer uma Dashboard melhor
    render "dashboard_default"
    # render "dashboard_no_educational_center",locals:{content:content}  if current_member.role.educator?  && current_member.educational_center.nil? 
     

      # columns do
      #   column do
      #     panel "Informações" do
      #       p "Desculpe, você não está cadastrado em nenhum centro educational" if current_member.educational_center.nil?
      #       p "Desculpe, seu usuário está desativado, entre em contato com o administrador do sistema" if current_member.status==false
      #     end
      #   end
      # end

    
  end # content
end
