class Profile < ActiveRecord::Base
  belongs_to :member
  belongs_to :child
  has_many :answers, as: :answerable
  accepts_nested_attributes_for :answers
  after_initialize :create_answers
  after_save :on_after_save

  acts_as_taggable


  def name 
    return I18n.t(:profile,scope:"active_admin.names", year: self.created_at.year, name:self.child.name)
  end
  def on_after_save 
    self.answers.each  do |answer|
      answer.answerable = self
      answer.child = self.child
      answer.save
    end
  end

  def questions
    Question.where(use_in_profile:true)
  end



  def create_answers
    # unless self.new_record?
      if self.answers.size == 0 then
      
        all_questions = Question.where(use_in_historic: true)
        all_questions.each do |question|
            self.answers.build(question: question, answerable: self)
        end 
      end
    # end
  end
  
end
