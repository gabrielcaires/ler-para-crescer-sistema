require 'test_helper'

class ProfileTest < ActiveSupport::TestCase
  include FactoryGirl::Syntax::Methods

  test 'the automatic creation of profile' do 
    before_create = Profile.all.count
    child = create(:child)
    assert_not_equal(before_create, Profile.all.count)
    assert_not_nil(child.profile)
  end

  test 'the automatic destruction of profile' do 
    before_create = Profile.all.count
    child = create(:child)
    child.destroy
    assert_equal(before_create, Profile.all.count)
  end

  test 'the search by questions of profile' do 
    child = create(:child)
    assert_equal child.profile.questions.count, Question.where(use_in_profile:true).count
  end

  
 
end
