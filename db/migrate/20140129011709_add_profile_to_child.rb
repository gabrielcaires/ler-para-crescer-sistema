class AddProfileToChild < ActiveRecord::Migration
  def change
    add_reference :children, :profile, index: true
  end
end
