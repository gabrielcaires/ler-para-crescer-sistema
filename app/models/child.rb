class Child < ActiveRecord::Base
	
	has_many :historics,  dependent: :destroy
	has_many :members, through:  :educational_center
	has_many :profiles,  dependent: :destroy
	belongs_to :educational_center
	has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" } #, :default_url => "/images/:style/missing.png"
	has_paper_trail


	# State Machine
	state_machine :initial => :ok do
	    event :historic_warnning do 
	    	ChildMailer.welcome_email()
	    end

	    event :profile_warnning do 
	    	ChildMailer.welcome_email()
	    end
	end

	def self.send_email 
		ChildMailer.welcome_email()
	end



	# Scopes
	scope :actives, ->{where(status:true)}
	scope :unactives, ->{where(status:false)}
	

	# Validations
	validates :name, presence: true
	validates :responsible, presence: true
	validates :address, presence: true
	validates :status, presence: true
	validates :birthdate, presence: true
	# Callbacks
	# after_save :create_profile

	# True each 1 year of last created profile
	def ready_to_new_profile?	
		return true if self.profiles.last.nil? 
		return Date.today > self.profiles.last.created_at.years_since(1) ? true :false
	end

	# True each 3 months of last created historic
	def ready_to_new_historic?
		return true if self.historics.last.nil? 
		return Date.today > self.historics.last.created_at.months_since(3) ? true :false
	end

	

	# For DSL of active_admin
	def profile_answers
		return self.profile.answers
	end

	def compact_name 
		return self.name.split(" ").first.camelize
	end
	
	

	def age
	 if self.birthdate
	  now = Time.now.utc.to_date
	  now.year - birthdate.year - (birthdate.to_date.change(:year => now.year) > now ? 1 : 0)
	 end
	end

	def active
		self.status = true
		self.save
	end

	def deactive
		self.status = false
		self.save
	end



end
