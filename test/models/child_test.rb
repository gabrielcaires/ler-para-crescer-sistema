require 'test_helper'

class ChildTest < ActiveSupport::TestCase
  include FactoryGirl::Syntax::Methods

  test 'the creation' do
    child = build(:child)
    assert child.save
  end

  test 'the validations' do
    assert_not build(:child, responsible:nil).save
    assert_not build(:child, address:nil).save
    assert_not build(:child, image_rights:nil).save
    assert_not build(:child, status:nil).save
  end

  test 'the creation of profile' do 
    child = create(:child)
    assert_not_nil(child.profile)
  end
 

end
