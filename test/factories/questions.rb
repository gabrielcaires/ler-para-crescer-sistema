# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :question do
    sequence(:label) { |n| "Pergunta #{n}?" }
    use_in_profile true
    use_in_historic true
    status true
  end
end
