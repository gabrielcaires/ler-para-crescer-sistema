require 'test_helper'

class QuestionTest < ActiveSupport::TestCase
	include FactoryGirl::Syntax::Methods
 
 

  test 'the validations' do
  	assert build(:question).save
  	 
  end

  test 'the presence on historic or profile' do 
  	assert_not build(:question, use_in_profile: false, use_in_historic:false).save
  end

  test 'the scope in_profile' do 
  	 before = Question.of_profile.count
  	 create(:question, use_in_profile: true)
  	 assert_equal(before+1, Question.of_profile.count)
  end

  test 'the scope in_historic' do 
  	before = Question.of_historic.count
  	create(:question, use_in_historic: true)
  	assert_equal(before+1, Question.of_historic.count)
  end

  test 'the answers of question' do 
    question = create(:question,use_in_profile:true)
    question.answers << create(:answer_profile)
    question.save!
    assert_equal(1, question.answers(true).count)
  end

end
