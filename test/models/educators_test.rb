require 'test_helper'

class MembersTest < ActiveSupport::TestCase
  include FactoryGirl::Syntax::Methods

  test 'the creation' do
    child = build(:member)
    assert child.save
  end

  test 'the validations' do
    assert_not build(:member, name:nil).save
    assert_not build(:member, address:nil).save
    assert_not build(:member, phone:nil).save
    assert_not build(:member, birthdate:nil).save
  end

 test 'the unniquess' do 
    build(:member, email:'email@email.com').save
    assert_not build(:member, email:'email@email.com').save
 end

end

