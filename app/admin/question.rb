ActiveAdmin.register Question do

  belongs_to :historic, parent_class: Historic,:optional => true

  permit_params :label, :use_in_profile, :use_in_historic
  
  # Scopes
  scope :actives, :default=> true
  scope :unactives
  scope :all

 # Batch actions 
   batch_action :active, :priority=> 0, :if => proc {params['scope'] != "actives"} do |selection|
     Question.find(selection).each do |item|
       item.active
     end
     redirect_to :back 
   end

   batch_action :deactive, :priority=> 1, :if => proc {params['scope'] != "unactives"} do |selection|
     Question.find(selection).each do |item|
       item.deactive
     end
     redirect_to :back 
   end

   batch_action :destroy, :if => proc {current_member.admin?}
  
  # Index
  index do
    selectable_column
    column :label
    column :answers do |question|
      if question.answers.count > 0 
        link_to question.answers.count, admin_question_answers_path(question)
      else
        question.answers.count
      end
    end
    
    actions do |d|
      link_to "Respostas", admin_question_answers_path(d)
    end

  end

  show do |question|
    attributes_table do
      row :label
      row :use_in_profile
      row :use_in_profile
      row :created_at
      row :updated_at
      row :answers do 
        link_to question.answers.count, admin_question_answers_path(question)
      end
    end
  end


  form do |f|
    f.inputs :label, :use_in_profile, :use_in_historic
    f.actions
  end
  
end
