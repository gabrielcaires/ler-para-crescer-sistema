# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :member do
	name {generate(:name)}
  	email {generate(:email)}
  	password '12345678'
  	address 'Rua XXXXXXXXXXXXXXXXXXXXXXXXXX'
  	phone '(xx) XXXXX-XXXX'
  	birthdate {Time.now()}
  	admin true
  end
end

