class EducationalCenter < ActiveRecord::Base
	has_many :children
	belongs_to :member
	has_paper_trail

	# Scopes
	scope :actives, ->{where(status:true)}
	scope :unactives, ->{where(status:false)}

	def active
		self.status = true
		self.save
	end

	def deactive
		self.status = false
		self.save
	end

end
