FactoryGirl.define do
  sequence :email do |n|
    "person#{n}@example.com"
  end
  sequence :name do |n|
    "Fulana #{n}"
  end

  sequence :address do |n|
    "Avenida #{n}"
  end
  
end