class Historic < ActiveRecord::Base
  belongs_to :member
  belongs_to :child
  belongs_to :educational_center
  has_many :answers, :as => :answerable
  accepts_nested_attributes_for :answers
  acts_as_taggable 

  # validates :tag_list, presence: true
  validates :member, presence: true
  after_initialize :create_answers
  after_save :on_after_save

  def on_after_save 
    self.answers.each  do |answer|
      answer.answerable = self
      answer.save
    end
  end



  def create_answers
    # unless self.new_record?
      if self.answers.size == 0 then
      
        all_questions = Question.where(use_in_profile: true)
        all_questions.each do |question|
          # if self.answers.where(answerable_id: question.id, answerable_type: self.class.name).count ==0 then
            self.answers.build(question: question, answerable: self)
          # end        
        end 
        # self.save
      end
    # end
  end
  
  	
  def name 
    return I18n.t(:historic,scope:"active_admin.names", date: self.created_at.strftime('%b/%Y'),year: self.created_at.year, name:self.child.name)
  end
end
