# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :child do
  	
  	sequence(:name) { |n| "Criança #{n}" }
  	responsible {generate(:name)}
  	birthdate {Time.now()}
  	address  {generate(:address)}
  	image_rights  true
  end
end
