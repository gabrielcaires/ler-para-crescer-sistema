# module ActiveAdmin::ViewHelpers
#   def active_status_tag (status_value)
#    status = ( status_value ? :ok : :gray)
#    label = status_value ? "Ativo =]" : "Não ativo =("
#    status_tag( label, status)
#   end 
# end

module ActiveAdmin
  module Views
    # Build a StatusTag
    class ActiveStatusTag < ActiveAdmin::Component
      builder_method :active_status_tag

      def tag_name
        'span'
      end

      def default_class_name
        'status_tag'
      end

      # @method status_tag(status, type = nil, options = {})
      #
      # @param [String] status the status to display. One of the span classes will be an underscored version of the status.
      # @param [Symbol] type type of status. Will become a class of the span. ActiveAdmin provide style for :ok, :warning and :error.
      # @param [Hash] options such as :class, :id and :label to override the default label
      #
      # @return [ActiveAdmin::Views::StatusTag]
      #
      # Examples:
      #   status_tag('In Progress')
      #   # => <span class='status_tag in_progress'>In Progress</span>
      #
      #   status_tag('active', :ok)
      #   # => <span class='status_tag active ok'>Active</span>
      #
      #   status_tag('active', :ok, :class => 'important', :id => 'status_123', :label => 'on')
      #   # => <span class='status_tag active ok important' id='status_123'>on</span>
      #
      def build(*args)
        options = args.extract_options!
        status = args[0]
        type = status ? :ok : :red
        label = options.delete(:label)
        classes = options.delete(:class)

        content = (status ? I18n.t("active_admin.active_status_tag.active") : I18n.t("active_admin.active_status_tag.no_active"))

        super(content, options)

        add_class(status_to_class(status.to_s)) if status
        add_class(type.to_s) if type
        add_class(classes) if classes
      end

      protected

      def status_to_class(status)
        status.titleize.gsub(/\s/, '').underscore
      end
    end
  end
end