ActiveAdmin.register EducationalCenter do
  # Permissions
  permit_params :name, :address, :member_id, :status
  navigation_menu :default

  # Filters
  filter :name
  filter :member 
  

  scope :actives
  scope :unactives
  scope :all

  # Batch actions 
  batch_action :active, :priority=> 0, :if => proc {params['scope'] != "actives"} do |selection|
    EducationalCenter.find(selection).each do |item|
      item.active
   
    end
    redirect_to :back 
  end

  batch_action :deactive, :priority=> 1, :if => proc {params['scope'] != "unactives"} do |selection|
    EducationalCenter.find(selection).each do |item|
      item.deactive
    end
    redirect_to :back 
  end
  batch_action :destroy, :if => proc {current_member.admin?}
    

  # Views
  index :download_links => [:csv, :xml, :json] do |educational_center|
    selectable_column
    column :name
    column :address
    column "QTD Crianças" do |d|
      status_tag( d.children.actives.count.to_s,:gray)
    end
    column :status do |educational_center|
        active_status_tag (educational_center.status)
    end
    actions do |d|
          link_to "Lista de Crianças", admin_educational_center_children_path(d)
    end
  end   
  
  show do |educational_center|
    attributes_table do
      row :name
      row :member
      row :address
      row :status do
          active_status_tag (educational_center.status)
      end
      row :children do |ed|
        link_to "Lista de Crianças", admin_educational_center_children_path(ed)
      end
    end
  end

 form :multipart => true  do |f|
      f.inputs "Informações" do
        f.input :name
        f.input :member
        f.input :address
        f.input :status
      end
      f.actions
  end   
end
