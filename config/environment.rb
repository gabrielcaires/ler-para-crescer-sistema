# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
# Mailer config external file
Crescer::Application.config.action_mailer.delivery_method = :smtp
Crescer::Application.config.action_mailer.smtp_settings = YAML.load_file(Rails.root.join('config', 'mailer.yml'))[Rails.env].to_options
Crescer::Application.initialize!
