require 'test_helper'

class HistoricTest < ActiveSupport::TestCase
  include FactoryGirl::Syntax::Methods

  test 'the automatic creation of historics' do 
    child = build(:child)
    child.historics << build(:historic)
    assert child.save
    assert_equal(1,child.historics.count)
    assert_equal(1, Historic.all.count)
    
  end


  test 'the automatic destruction of historics' do 
    child = build(:child)
    child.historics << build(:historic)
    assert child.save
    before_destory = Historic.all.count
    child.destroy
    assert_equal(before_destory-1, Historic.all.count)
  end

  test 'the list of questions of historic' do 
    child = build(:child)
    child.historics << create(:historic)
    list = create_list(:answer_historic, 10, answerable:child.historics.first)
    child.historics << build(:historic)
    assert_equal(10, child.historics.first.answers.count)
  end

end
