require 'test_helper'

class EducationalCenterTest < ActiveSupport::TestCase
  include FactoryGirl::Syntax::Methods

  test 'the creation' do
    child = build(:educational_center)
    assert child.save
  end

  test 'the validations' do
    assert_not build(:educational_center, name:nil).save
    assert_not build(:educational_center, address:nil).save
  end

end

