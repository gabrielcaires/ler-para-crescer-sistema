# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :educational_center do
  	name "Centro Educational"
  	address {"Enderesso #{name}" }
  	status true
	member
  	
  end
end
