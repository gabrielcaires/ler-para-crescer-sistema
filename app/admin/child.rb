ActiveAdmin.register Child do
  belongs_to :educational_center, parent_class: EducationalCenter,:optional =>true
  permit_params :name, :educational_center_id,:avatar, :address, :birthdate, :responsible, :image_rights, :school, :year_school, :active
  config.comments = false
  navigation_menu :default
 
  # menu parent: "Centros de Educação"

  filter :name
  filter :birthdate
  filter :school
  filter :status
  filter :address
  filter :responsible
  filter :image_rights
  scope :actives
  scope :unactives
  scope :all

  # Batch actions 
  batch_action :active, :priority=> 0, :if => proc {params['scope'] != "actives"} do |selection|
    Child.find(selection).each do |item|
      item.active
   
    end
    redirect_to :back 
  end

  batch_action :deactive, :priority=> 1, :if => proc {params['scope'] != "unactives"} do |selection|
    Child.find(selection).each do |item|
      item.deactive
    end
    redirect_to :back 
  end

  batch_action :destroy, :if => proc {current_member.admin?}
  
  
  index do
    selectable_column
    column :avatar do |child|
      image_tag (child.avatar.url(:thumb)) unless child.avatar.blank?
    end
   
    column :name
    column :educational_center if params[:educational_center_id].nil?
    column :age
    column :status do |child|
      if child.ready_to_new_profile? then
         link_to 'Atualizar Perfil', new_admin_child_profile_path( child), {class:"status_tag red"}
      elsif child.ready_to_new_historic? then
        link_to 'Atualizar Histórico', new_admin_child_historic_path( child), {class:"status_tag red"}
      else
        active_status_tag(child.status)
      end
    end

    actions 
    actions defaults: false do |child|
      unless child.educational_center.nil?
        link_to "Perfil",  admin_educational_center_child_profiles_path(educational_center_id:child.educational_center.id,child_id:child.id)
      else
        link_to "Perfil",  admin_child_profiles_path(child)
       
      end
    end
    actions defaults: false do |child|
      unless  child.educational_center.nil?
        link_to "Históricos",  admin_educational_center_child_historics_path(educational_center_id:child.educational_center.id,child_id:child.id)
      else
        link_to "Históricos",  admin_child_historics_path(child)
      end
    end
  end
 

  show do |child|
        attributes_table do
       
          row :avatar do
            image_tag(child.avatar.url(:thumb)) unless child.avatar.blank?
          end
          row :educational_center
          row :name
          row :address
          row :birthdate
          row :age
          row :responsible
          row :image_rights do
              content_tag(:p, child.image_rights ? "Sim" : "Não")
          end

          row :school
          row :year_school
          row :status do
              active_status_tag(child.status)
          end

        end


        # div do
        #   panel "Histórico" do
        #   text_node link_to "Novo histórico", new_admin_child_historic_path(child)
        #     if child.historics.size > 0
        #       table_for child.historics.limit(10) do
        #       end 
        #     else
        #       content_tag(:p, "Ainda não temos nenhum histórico")
        #     end
        #   end
        # end
       
  end

   form do |f|
      f.inputs "Informações" do |child|
        f.input :avatar, :as => :file, :hint => (f.template.image_tag(f.object.avatar.url(:thumb)) unless f.object.avatar.blank?)
        f.input :educational_center
        f.input :name
        f.input :address
        f.input :birthdate, :start_year=> 1900
        f.input :member_id, as:"hidden", input_html:{value:current_member.id}
        f.input :responsible
        f.input :image_rights
        f.input :school
        f.input :year_school
        f.input :status
        f.input :created_at, as: 'date'
        
        # debugger 
      #   f.input :profile do |profile_form|
      #       profile_form.has_many :answers do |answer_form|
      #           answer_form.input :value
      #       end
      #   end
      # end

      # f.inputs "Profile Details", for: :profile do |profile_form|
      #   profile_form.has_many :answers do |answer_form|
      #     answer_form.input :value
      #   end
       
      # end
         # f.has_many :answers,new_record: false, new_record:false, allow_destroy:false do |fa|
         #      fa.input :question, :input_html => { :disabled => true } do 
         #        fa.question.label
         #      end
         #      # fa.input :answerable
         #      # fa.input :id, :as=> :hidden 
         #      fa.input :value
         #      fa.input :member_id, :as=> :hidden, :input_html => {:value => current_member.id}
         #      fa.input :question_id,:as=> :hidden do 
         #         fa.question.id
         #      end

         #    end
         #    f.input :tags, :as => :check_boxes
      
      f.actions
      end
    end

end

