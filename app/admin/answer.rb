ActiveAdmin.register Answer do
  belongs_to :historic, parent_class: Historic ,:optional => true
  menu false
  permit_params :answer, :historic, :profile, :value, :member_id
  
  filter :question
  filter :use_in_historic
  filter :answerable_type, label: "Pergunta usada em:"
  filter :child
  filter :created_at
  filter :updated_at
  index do
    selectable_column
    column :question do |answer|
      answer.question.label
    end
    column :answered? do |answer|
      active_status_tag(answer.answered?)
    end
    default_actions
  end

  form do |f|
    f.inputs "Informações" do
      f.input :question , input_html:{disabled:true}
      f.input :value
      f.input :member_id, :as=> :hidden, :value => current_member.id
    end
    f.actions
  end
  
end
