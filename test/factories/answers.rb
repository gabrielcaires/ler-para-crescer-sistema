# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :answer do
    value "Resposta historic"
    association :question, factory: :question, use_in_historic:true
    association :answerable, factory: :historic
    # association :member, factory: :member
    
    factory :answer_historic do
      value "Resposta historic"
      association :question, factory: :question, use_in_historic:true
      association :answerable, factory: :historic
      # association :member, factory: :member
    end

    factory :answer_profile do
      value "Resposta profile"
      association :question, factory: :question, use_in_profile:true
      association :answerable, factory: :profile
      # association :member, factory: :member
    end

  end
end
