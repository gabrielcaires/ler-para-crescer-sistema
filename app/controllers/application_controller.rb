class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def user_for_paper_trail
      admin_user_signed_in? ? current_member : 'Usuário desconhecido'
   end


   def access_denied(exception)
   	
     redirect_to admin_educational_centers_path, :alert => exception.message
   end
end
