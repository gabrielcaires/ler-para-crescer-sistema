class Question < ActiveRecord::Base
	# SCOPE
	scope :of_profile, ->(){where(use_in_profile:true)}
	scope :of_historic, ->(){where(use_in_historic:true)}
	scope :actives, ->{where(status:true)}
	scope :unactives, ->{where(status:false)}
	
	has_many :answers, :as => :answerable, :foreign_key =>:answerable_id
	
	# Callbacks
	after_validation :check_profile_or_historic


	# Verify if one of values is 'true'
	def check_profile_or_historic
		if self.use_in_profile == false && self.use_in_historic==false then 
			errors.add(:use_in_historic,"A pergunta deve estar em pelo menos uma das opções")
			errors.add(:use_in_profile,"A pergunta deve estar em pelo menos uma das opções")
		end
	end


	# 
	def active
		self.status = true
		self.save
	end

	# 
	def deactive
		self.status = false
		self.save
	end

	def name 
		self.label
	end
end
