# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140130010434) do

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "answers", force: true do |t|
    t.text     "value"
    t.integer  "profile_id"
    t.integer  "historic_id"
    t.integer  "answerable_id"
    t.string   "answerable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "question_id"
    t.integer  "member_id"
    t.integer  "child_id"
  end

  add_index "answers", ["child_id"], name: "index_answers_on_child_id", using: :btree
  add_index "answers", ["historic_id"], name: "index_answers_on_historic_id", using: :btree
  add_index "answers", ["profile_id"], name: "index_answers_on_profile_id", using: :btree
  add_index "answers", ["question_id"], name: "index_answers_on_question_id", using: :btree
  add_index "answers", ["member_id"], name: "index_answers_on_member_id", using: :btree

  create_table "children", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.date     "birthdate"
    t.string   "responsible"
    t.boolean  "image_rights"
    t.string   "school"
    t.string   "year_school"
    t.boolean  "status",                default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "educational_center_id"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.integer  "profile_id"
  end

  add_index "children", ["educational_center_id"], name: "index_children_on_educational_center_id", using: :btree
  add_index "children", ["profile_id"], name: "index_children_on_profile_id", using: :btree

  create_table "educational_centers", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.boolean  "status",       default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "member_id"
  end

  add_index "educational_centers", ["member_id"], name: "index_educational_centers_on_member_id", using: :btree

  create_table "historics", force: true do |t|
    t.integer  "member_id"
    t.integer  "child_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "educational_center_id"
  end

  add_index "historics", ["child_id"], name: "index_historics_on_child_id", using: :btree
  add_index "historics", ["educational_center_id"], name: "index_historics_on_educational_center_id", using: :btree
  add_index "historics", ["member_id"], name: "index_historics_on_member_id", using: :btree

  create_table "profiles", force: true do |t|
    t.integer  "child_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "member_id"
  end

  add_index "profiles", ["child_id"], name: "index_profiles_on_child_id", using: :btree
  add_index "profiles", ["member_id"], name: "index_profiles_on_member_id", using: :btree

  create_table "questions", force: true do |t|
    t.string   "label"
    t.boolean  "use_in_profile"
    t.boolean  "use_in_historic"
    t.boolean  "status",          default: true
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "taggings", force: true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree

  create_table "tags", force: true do |t|
    t.string "name"
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "versions", force: true do |t|
    t.string   "item_type",  null: false
    t.integer  "item_id",    null: false
    t.string   "event",      null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  create_table "members", force: true do |t|
    t.string   "name"
    t.string   "address"
    t.date     "birthdate"
    t.string   "phone"
    t.date     "start_date"
    t.date     "end_date"
    t.boolean  "status",                 default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "email"
    t.string   "facebook"
    t.string   "cell1"
    t.string   "cell2"
    t.string   "cell3"
    t.string   "avatar_file_name"
    t.string   "avatar_content_type"
    t.integer  "avatar_file_size"
    t.datetime "avatar_updated_at"
    t.string   "encrypted_password",     default: "",   null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,    null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.boolean  "admin"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.text     "description"
    t.string   "role"
  end

  add_index "members", ["email"], name: "index_members_on_email", unique: true, using: :btree
  add_index "members", ["reset_password_token"], name: "index_members_on_reset_password_token", unique: true, using: :btree

end
