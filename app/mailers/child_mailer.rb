class ChildMailer < ActionMailer::Base
  default from: "gabrielcairesmar@gmail.com"

   def welcome_email()
    
    @url  = 'http://example.com/login'
    mail(to: 'gabrielcairesmar@gmail.com', subject: 'Welcome to My Awesome Site')
  end

end
