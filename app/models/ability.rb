class Ability
    include CanCan::Ability
    def initialize(user)

    	can :read, ActiveAdmin::Page, :name => "Dashboard"
    	case user.role.to_sym
	    	when :nothing  
	    		return false
	    		
	    	when :educator
		    	can [:read, :update], Member, :id => user.id
	    		unless user.educational_center.nil? then
					can [:create,:update, :read], Answer 
					can [:create,:update, :read], Profile
					can [:create,:update,:read], Historic, :educational_center_id => user.educational_center.id
					can [:create,:update, :read], Child,  :educational_center_id => user.educational_center.id
		    	end

		    when :human_resources
		    	can :manage, Member

		    when :admin 
		    	can :manage, :all

		    when :psychologist
		    	can [:read, :update], Member, :id => user.id
		    	can [:read], [Child, Profile, Historic, EducationalCenter]

		    
    	end 
    end

 end