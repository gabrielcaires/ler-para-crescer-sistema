Crescer::Application.routes.draw do
  devise_for :members, ActiveAdmin::Devise.config
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  namespace :admin do
    resources :children do 
        resources :profiles
        resources :historics
    end
    resources :questions do 
      resources :answers
    end
    resources :members
    resources :historics
    resources :educational_centers do 
      resources :children do 
        resources :profiles
        resources :historics
      end
    end
  end

  # match "/admin/educational_centers/:educational_center_id/children/:child_id/profiles/" => "admin/profile#index", via: "get", as: "admin_educational_center_child_profile"  
  # match "/admin/educational_centers/:educational_center_id/children/:child_id/profiles/:profile_id" => "admin/profile#show",  as: "admin_educational_center_child_profile"  
 
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
end
