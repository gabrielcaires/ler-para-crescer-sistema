class Member < ActiveRecord::Base
  	extend Enumerize
  	# Devive Rules
  	devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable

    # Roles of access
    enumerize :role, in: [:nothing, :educator, :admin, :human_resources, :psychologist], default: :nothing
	
	# Associations
	has_one :educational_center

	# Scopes
	scope :actives, ->{where(status:true)}
	scope :unactives, ->{where(status:false)}
	
	# Others
	acts_as_taggable_on :skills
	has_paper_trail
	has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" } ,  
	                      :url => "/uploads/members/:id/images/:basename.:extension",  
	                      :path => ":rails_root/public/members/images/:id/:basename.:extension"

	has_attached_file :file,:url => "/uploads/members/:id/documents/:basename.:extension",  
	                        :path => ":rails_root/public/uploads/members/documents/:id/:basename.:extension"
	                      
	
	# Validations
	validates :name, presence: true
	validates :skils, presence: true
	# validates :address, presence: true
	# validates :phone, presence: true
  	# validates :birthdate, presence: true
  	validates_attachment_size :avatar, :less_than=>2.megabyte, :unless => Proc.new { |imports| imports.avatar_file_name.blank? }
	validates_attachment_content_type :avatar, :content_type=>['image/jpeg', 'image/png', 'image/gif'] 
	validates_attachment_size :file, :less_than => 3.megabytes    
	validates_attachment_content_type :file, :content_type =>["application/pdf","application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "text/plain"],:message => ', Somente são permitidos PDF, WORD ou TEXT. '


	#Check is new image 
	after_validation :validate_image
	before_destroy :check_destroy
	before_update :check_update

	# Permit loggin for only active users
	#TODO CUSTOM ERROR MESSAGE
	def self.find_first_by_auth_conditions(w_conditions)
      	conditions = w_conditions.dup
        return where(conditions).where.not(role: "nothing", status: false).first
    end

	
	# Verify if has one admin user on member attribute editing
	def check_update
		if self.admin_changed? && self.admin? == false
			if Member.where(:admin=>true).where.not(:id=>self.id).count < 1 
				self.admin=true
				errors.add(:admin,"O sistema precisa de pelo menos 1 administrador")
				return false
			end
		end

	end

	# Verify if has one admin user on member destroy
	def check_destroy
		if Member.where(:admin=>true).where.not(:id=>self.id).count < 1
			errors.add(:admin,"O sistema precisa de pelo menos 1 administrador")
			return false
		end
	end

	# Verify if has a valid image
	def validate_image 
		if self.avatar.nil? then return true end
 		if self.avatar.errors.empty? then
			return true
		end
		return false
	end

	# 
	def active
		self.status = true
		self.save
	end

	# 
	def deactive
		self.status = false
		self.save
	end

	def first_name
		return self.name.split(' ')[0]
	end
end