-- MySQL dump 10.13  Distrib 5.5.32, for debian-linux-gnu (i686)
--
-- Host: localhost    Database: crescer
-- ------------------------------------------------------
-- Server version	5.5.32-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
INSERT INTO schema_migrations (version) VALUES ('20140115164356');

INSERT INTO schema_migrations (version) VALUES ('20140115170151');

INSERT INTO schema_migrations (version) VALUES ('20140115190148');

INSERT INTO schema_migrations (version) VALUES ('20140115211850');

INSERT INTO schema_migrations (version) VALUES ('20140116185420');

INSERT INTO schema_migrations (version) VALUES ('20140116185658');

INSERT INTO schema_migrations (version) VALUES ('20140116195013');

INSERT INTO schema_migrations (version) VALUES ('20140116201134');

INSERT INTO schema_migrations (version) VALUES ('20140116215752');

INSERT INTO schema_migrations (version) VALUES ('20140117175208');

INSERT INTO schema_migrations (version) VALUES ('20140117181525');

INSERT INTO schema_migrations (version) VALUES ('20140120023932');

INSERT INTO schema_migrations (version) VALUES ('20140120035045');

INSERT INTO schema_migrations (version) VALUES ('20140120045007');

INSERT INTO schema_migrations (version) VALUES ('20140120045335');

INSERT INTO schema_migrations (version) VALUES ('20140120045336');

INSERT INTO schema_migrations (version) VALUES ('20140120053148');

INSERT INTO schema_migrations (version) VALUES ('20140122222835');

INSERT INTO schema_migrations (version) VALUES ('20140122224613');
