class AddMemberToProfile < ActiveRecord::Migration
  def change
    add_reference :profiles, :member, index: true
  end
end
