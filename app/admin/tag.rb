ActiveAdmin.register Tag do
  belongs_to :historic, parent_class: Historic , :optional => true
  menu false
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end


  # Views
  index do 
    column :name
    column :count do |tag|
      Historic.tagged_with(tag.name).count
    end
  end
  
end
